import matplotlib.pyplot as plt
import numpy as np
import subprocess

OUTFILE = "/tmp/llraodhusnaoh.png"

n=10
x=np.random.randn(n)
y=np.random.randn(n)

plt.plot(x, y)
plt.savefig(OUTFILE)

subprocess.run(["kitty", "+icat", OUTFILE])
