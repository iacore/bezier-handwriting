const std = @import("std");

pub const Point = struct {
    x: f32,
    y: f32,

    pub fn init(x: f32, y: f32) @This() {
        return .{ .x = x, .y = y };
    }

    pub fn jsonParse(alloc: std.mem.Allocator, source: anytype, options: std.json.ParseOptions) !@This() {
        return .{
            .x = value[0],
            .y = value[1],
        };
    }
    pub fn jsonStringify(this: @This(), jws: anytype) !void {
        try jws.write(.{ this.x, this.y });
    }
};

pub const Result = struct {
    knots: []const Point, // .len=n
    ctrlPoint1: std.ArrayList(Point), // .len=n-1
    ctrlPoint2: std.ArrayList(Point), // .len=n-1

    pub fn deinit(this: @This()) void {
        this.ctrlPoint1.deinit();
        this.ctrlPoint2.deinit();
    }

    // this->path.moveTo(knots[0]);
    // for(int i=0;i<n;i++)
    // {
    //     this->path.cubicTo(ctrlPoint1[i],ctrlPoint2[i],knots[i+1]);
    // }
};

fn createPointList(alloc: std.mem.Allocator, n: usize) !std.ArrayList(Point) {
    var arr = try std.ArrayList(Point).initCapacity(alloc, n);
    errdefer arr.deinit();
    try arr.resize(n);
    return arr;
}

pub fn do(alloc: std.mem.Allocator, knots: []const Point) !Result {
    const n = knots.len - 1;
    var rsp = try createPointList(alloc, n);
    defer rsp.deinit();
    {
        var i: usize = 1;
        while (i + 2 <= n) : (i += 1) {
            rsp.items[i].x = (4 * knots[i].x + 2 * knots[i + 1].x);
            rsp.items[i].y = (4 * knots[i].y + 2 * knots[i + 1].y);
        }
    }
    rsp.items[0].x = (knots[0].x + 2 * knots[1].x);
    rsp.items[0].y = (knots[0].y + 2 * knots[1].y);
    rsp.items[n - 1].x = ((8 * knots[n - 1].x + knots[n].x) / 2.0);
    rsp.items[n - 1].y = ((8 * knots[n - 1].y + knots[n].y) / 2.0);

    var internal = try createPointList(alloc, n);
    defer internal.deinit();
    var temp = try createPointList(alloc, n);
    defer temp.deinit();

    var b1: f32 = 2.0;
    var b2: f32 = 2.0;
    internal.items[0].x = (rsp.items[0].x / b1);
    internal.items[0].y = (rsp.items[0].y / b2);
    for (1..n) |i| {
        temp.items[i].x = (1 / b1);
        temp.items[i].y = (1 / b2);
        b1 = (if (i < n - 1) @as(f32, 4.0) else 3.5) - temp.items[i].x;
        b2 = (if (i < n - 1) @as(f32, 4.0) else 3.5) - temp.items[i].y;

        internal.items[i].x = ((rsp.items[i].x - internal.items[i - 1].x) / b1);
        internal.items[i].y = ((rsp.items[i].y - internal.items[i - 1].y) / b2);
    }
    for (1..n) |i| {
        internal.items[n - i - 1].x = (internal.items[n - i - 1].x - temp.items[n - i].x * internal.items[n - i].x);
        internal.items[n - i - 1].y = (internal.items[n - i - 1].y - temp.items[n - i].y * internal.items[n - i].y);
    }

    var ctrlPoint1 = try createPointList(alloc, n);
    errdefer ctrlPoint1.deinit();
    var ctrlPoint2 = try createPointList(alloc, n);
    errdefer ctrlPoint2.deinit();

    for (1..n) |i| {
        ctrlPoint1.items[i - 1] = (internal.items[i]);
        if (i < n - 1) {
            ctrlPoint2.items[i - 1] = (Point.init(2 * knots[i + 1].x - internal.items[i + 1].x, 2 *
                knots[i + 1].y - internal.items[i + 1].y));
        } else {
            ctrlPoint2.items[i - 1] = (Point.init((knots[n].x + internal.items[n - 1].x) / 2, (knots[n].y + internal.items[n - 1].y) / 2));
        }
    }

    return .{
        .ctrlPoint1 = ctrlPoint1,
        .ctrlPoint2 = ctrlPoint2,
        .knots = knots,
    };
}

pub fn main() !void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    defer _ = gpa.deinit();
    const a = gpa.allocator();

    var reader = std.json.reader(a, std.io.getStdIn().reader());
    defer reader.deinit();
    const parsed = try std.json.parseFromTokenSource([]Point, a, &reader, .{});
    defer parsed.deinit();
    const result = try do(a, parsed.value);
    defer result.deinit();
    const stdout = std.io.getStdOut().writer();
    try std.json.stringify(.{ result.ctrlPoint1.items, result.ctrlPoint2.items }, .{}, stdout);
}
