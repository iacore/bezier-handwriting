# recurrent neural network based handwriting2vec

The designed use is handwriting recognition, but it may also learn other things from handwriting, for example, whose handwriting is it.



## Questions

Q: What is "convergence" (when fitting a cubic curve to any stroke)?  
A: I think I understand. It means "Alternate between 2 steps until the curve doesn't change much."


## References

Idea from [Fast multi-language LSTM-based online handwriting recognition](https://link.springer.com/content/pdf/10.1007/s10032-020-00350-4.pdf).  

WKV block, from [BlinkDL](https://github.com/BlinkDL), creator of [RWKV](https://github.com/BlinkDL/RWKV-LM) ([Website](https://www.rwkv.com/)).

(Unused) Points to bezier curve code 來自[浅云](https://github.com/shallowclouds/BezierCurve/). This code is not working for some reason.  
