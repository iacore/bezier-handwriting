const std = @import("std");
const ts = @import("tinyspline.zig");

pub fn main() !void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    defer _ = gpa.deinit();
    const a = gpa.allocator();

    var err: ts.tsError = 0;
    var out: ts.tsBSpline = undefined;
    err = ts.ts_bspline_new(4, 3, 1, ts.TS_CLAMPED, &out, null);
    if (err != 0) return error.TinySplineError;
    var ctrlp: [*]ts.tsReal = undefined;
    err = ts.ts_bspline_control_points(&out, @ptrCast(&ctrlp), null);
    if (err != 0) return error.TinySplineError;

    const len = ts.ts_bspline_len_control_points(&out);

    const ctrlslice = ctrlp[0..len];
    std.log.info("{any}", .{ctrlslice});

    _ = a;

    // var reader = std.json.reader(a, std.io.getStdIn().reader());
    // defer reader.deinit();
    // const parsed = try std.json.parseFromTokenSource([]Point, a, &reader, .{});
    // defer parsed.deinit();
    // const result = try smoothcurve(a, parsed.value);
    // defer result.deinit();
    // const stdout = std.io.getStdOut().writer();
    // try std.json.stringify(.{ result.ctrlPoint1.items, result.ctrlPoint2.items }, .{}, stdout);
}
