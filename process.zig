const std = @import("std");
const pow = std.math.pow;
const List = std.ArrayList;
const Allocator = std.mem.Allocator;

const Point = struct {
    x: f32,
    y: f32,
    t: f32,

    pub fn vector(this: @This()) @Vector(3, f32) {
        return .{ this.x, this.y, this.t };
    }

    pub fn init(v: @Vector(3, f32)) @This() {
        return .{
            .x = v[0],
            .y = v[1],
            .t = v[2],
        };
    }
};

/// a sequence of storkes
pub const Ink = struct {
    /// points of all stroke concat together
    points: []Point,
    /// start indices of strokes
    starts: []usize,

    /// normalize ink strokes in-place
    pub fn normalize(this: @This()) void {
        normalize_ink(this.points);
    }
};

/// Normalize ink
///
/// Algorithm based on Formula 2.1.2 (2)
/// The original paper self-contradicts here.
///
/// The algorithm works on multiple strokes (1 ink), but the paper says it should "normalize a stroke".
/// > This sets the time difference between the first and last points of the stroke to be equal to the total spatial length of the stroke.
///
/// I interpret this as normalizing the whole ink together, instead of normalizing the time component of each stroke individually.
pub fn normalize_ink(points: []Point) void {
    if (points.len == 0) return;

    var min_y: f32 = std.math.inf(f32);
    var max_y: f32 = -std.math.inf(f32);
    for (points) |p| {
        min_y = @min(min_y, p.y);
        max_y = @max(max_y, p.y);
    }
    const ratio_xy = 1 / (max_y - min_y);
    const p0x = points[0].x;
    for (points) |*p| {
        p.y = (p.y - min_y) * ratio_xy; // shrink y within [0, 1]
        p.x = (p.x - p0x) * ratio_xy; // x0 is 0
    }

    var sum_xy: f32 = 0;
    var sum_t: f32 = 0;
    for (0..points.len - 1) |i| {
        const p = points[i];
        const p1 = points[i + 1];
        sum_xy += @sqrt(pow(f32, p1.x - p.x, 2) + pow(f32, p1.y - p.y, 2));
        sum_t += p1.t - p.t;
    }
    const ratio_t = sum_xy / sum_t;

    const p0t = points[0].t;

    for (1..points.len) |i| {
        points[i].t = (points[i].t - p0t) * ratio_t; // shrink t within [0, ink length]
    }
}

test "normalize" {
    {
        var sample = [_]Point{
            Point.init(.{ 0, 0, 99 }),
            Point.init(.{ 1, 1, 100 }),
            Point.init(.{ 2, 0, 150 }),
            Point.init(.{ 3, 1, 200 }),
        };
        normalize_ink(&sample);
        try std.testing.expectApproxEqRel(@as(f32, 3 * @sqrt(2.0)), sample[sample.len - 1].t, std.math.floatEps(f32));
        try std.testing.expectApproxEqRel(@as(f32, 0), sample[0].x, std.math.floatEps(f32));
        for (sample) |p| {
            try std.testing.expect(p.y >= 0);
            try std.testing.expect(p.y <= 1);
        }
    }
}

pub const Curve = struct {
    //! Cubic bezier curve

    /// the stroke this curve is matching against
    /// borrowed
    stroke: []Point,

    a: Allocator,

    /// Control points
    /// \alpha \beta ... in paper
    ///
    /// when this doesn't change much after `update_*` steps, the curve has reached "convergence"
    controls: [4]Point,

    /// Where on the curve are the stroke points
    /// s_i in paper
    positions: List(f32),

    pub fn initFromStroke(a: Allocator, stroke: []Point) !@This() {
        if (stroke.len == 0) unreachable;
        const p0 = stroke[0].vector();
        const p1 = stroke[stroke.len - 1].vector();

        const controls = [4]Point{
            Point.init(p0),
            Point.init(p0 * (2 / 3) + p1 * (1 / 3)),
            Point.init(p0 * (1 / 3) + p1 * (2 / 3)),
            Point.init(p1),
        };
        var positions = List(f32).init(a);
        try positions.resize(stroke.len);
        var ret = @This(){
            .a = a,
            .stroke = stroke,
            .controls = controls,
            .positions = positions,
        };
        ret.update_pos();
        return ret;
    }
    pub fn uninit(this: @This()) void {
        this.positions.deinit();
    }
    pub fn clone(this: @This()) !@This() {
        var ret = this;
        ret.positions = try ret.positions.clone();
        return ret;
    }

    /// pick nearest s_i on the curve near each stroke point
    pub fn update_pos(this: *@This()) void {
        // todo
        _ = this;
    }

    pub fn update_curve(this: *@This()) void {
        // todo
        _ = this;
    }

    /// test if two curves are close
    pub fn test_close(this: @This(), that: @This()) bool {
        _ = that;
        _ = this;
    }

    /// Reason why the curve matched is bad
    pub const Unfit = enum {
        /// (a) the curve cannot fit the points well (SSE error is too large)
        too_inaccurate,
        /// (b) the curve has too sharp bends (arc length longer than 3 times the endpoint distance)
        too_twisty,
    };

    pub fn test_fit(this: @This()) ?Unfit {
        _ = this;
        // todo
    }

    /// split the curve in 2
    /// doesn't consume `this`
    pub fn split(this: @This(), why: Unfit) ![2]@This() {
        _ = why;
        _ = this;
    }

    /// merge two connected curves
    /// doesn't consume `this` or `that`
    pub fn splice(this: @This(), that: @This()) !@This() {
        _ = that;
        _ = this;
    }

    /// fit the curve; highest level API
    /// doesn't consume `this`
    pub fn fit(this: @This()) !union(enum) { ok, unfit: [2]@This() } {
        _ = this;
    }
};

// fn createPointList(alloc: std.mem.Allocator, n: usize) !List(Point) {
//     var arr = try List(Point).initCapacity(alloc, n);
//     errdefer arr.deinit();
//     try arr.resize(n);
//     return arr;
// }

pub fn main() !void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    defer _ = gpa.deinit();
    const a = gpa.allocator();
    _ = a;
    _ = Curve;
    // var reader = std.json.reader(a, std.io.getStdIn().reader());
    // defer reader.deinit();
    // const parsed = try std.json.parseFromTokenSource([]Point, a, &reader, .{});
    // defer parsed.deinit();
    // const result = try smoothcurve(a, parsed.value);
    // defer result.deinit();
    // const stdout = std.io.getStdOut().writer();
    // try std.json.stringify(.{ result.ctrlPoint1.items, result.ctrlPoint2.items }, .{}, stdout);
}
